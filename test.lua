--[[
 This is Russian description. English description see below.
 
 Часть тестов для пакета lua-gettext на языке Lua
 
 Copyright 2016 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 
 Этот файл -- часть пакета lua-gettext.
 
 О лицензии читай в файле lua-gettext.h
 
-----------------------------------------------------------------------
 
 Lua part of tests for lua-gettext package
 
 Copyright 2016 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 
 This file is part of lua-gettext package.
 
 See Copyright in lua-gettext.h

]]

local gettext = require "gettext"

-- 							default test						 --

assert ( gettext.gettext("Test string 1.1") == "Тестовая строка 1.1", 
		"Gettext don't translate string")

assert ( gettext.gettext("Untranslated string 1.1") == 
	"Untranslated string 1.1", 
		"Gettext translate untranslated string")

assert ( _("Test string 1.2") == "Тестовая строка 1.2", 
		"shorthand '_()' don't translate string")

assert ( _("Untranslated string 1.2") == 
	"Untranslated string 1.2", 
		"shorthand '_()' translate untranslated string")

assert ( gettext.ngettext("Test string 1.3", "Test stringS 1.3", 1) == 
	"Тестовая строка 1.3", 
		"Ngettext don't translate 1 form of string")

assert ( gettext.ngettext("Test string 1.3", "Test stringS 1.3", 2) == 
	"Тестовые строки 1.3", 
		"Ngettext don't translate 2 form of string")

assert ( gettext.ngettext("Test string 1.3", "Test stringS 1.3", 5) == 
	"Тестовых строк 1.3", 
		"Ngettext don't translate 3 form of string")

assert ( gettext.ngettext("Untranslated string 1.3", 
	"Untranslated stringS 1.3", 1) == 
	"Untranslated string 1.3", 
		"Ngettext translate 1 form of untranslated string")

assert ( gettext.ngettext("Untranslated string 1.3", 
	"Untranslated stringS 1.3", 12) == 
	"Untranslated stringS 1.3", 
		"Ngettext translate 2 form of untranslated string")


-- 							domain test							 --

local start_domain = gettext.textdomain (nil)

assert ( type(start_domain) == "string", "Textdomain return no string")

local new_domain = "mymsg"

gettext.bindtextdomain (new_domain, ".")

gettext.textdomain (new_domain)
assert ( gettext.gettext("Test string 2.1") == "Тестовая строка 2.1", 
		"Gettext don't translate string after domain changed")

assert ( gettext.gettext("Untranslated string 2.1") == 
	"Untranslated string 2.1", 
		"Gettext translate untranslated string after domain changed")

gettext.textdomain (start_domain)
assert ( gettext.gettext("Test string 1.1") == "Тестовая строка 1.1", 
		"Gettext don't translate string after domain comeback")

assert ( gettext.gettext("Untranslated string 1.1") == 
	"Untranslated string 1.1", 
		"Gettext translate untranslated string after domain comeback")

assert ( gettext.dgettext(new_domain, "Test string 2.1") == 
	"Тестовая строка 2.1", 
		"Dgettext don't translate string")

assert ( gettext.dgettext(new_domain, "Untranslated string 2.1") == 
	"Untranslated string 2.1", 
		"Dgettext translate untranslated string")

gettext.textdomain (new_domain)

assert ( gettext.dngettext(start_domain, 
	"Test string 1.3", "Test stringS 1.3", 1) == 
	"Тестовая строка 1.3", 
		"DNgettext don't translate 1 form of string")

assert ( gettext.dngettext(start_domain, 
	"Test string 1.3", "Test stringS 1.3", 2) == 
	"Тестовые строки 1.3", 
		"DNgettext don't translate 2 form of string")

assert ( gettext.dngettext(start_domain, 
	"Test string 1.3", "Test stringS 1.3", 5) == 
	"Тестовых строк 1.3", 
		"DNgettext don't translate 3 form of string")

assert ( gettext.dngettext(start_domain, 
	"Untranslated string 1.3", "Untranslated stringS 1.3", 1) == 
	"Untranslated string 1.3", 
		"DNgettext translate 1 form of untranslated string")

assert ( gettext.dngettext(start_domain, 
	"Untranslated string 1.3", "Untranslated stringS 1.3", 12) == 
	"Untranslated stringS 1.3", 
		"ENgettext translate 2 form of untranslated string")


-- 							category test							 --

local my_cat = "LC_NAME"
local cat_domain = "luacat"

gettext.bindtextdomain (cat_domain, ".")


assert ( gettext.dcgettext(cat_domain, "Test string 3.1", my_cat) == 
	"Тестовая строка 3.1", 
		"DCgettext don't translate string")

assert ( gettext.dcgettext(cat_domain, "Untranslated string 3.1", 
	my_cat) == 
	"Untranslated string 3.1", 
		"DCgettext translate untranslated string")

assert ( gettext.dcngettext(cat_domain, 
	"Test string 3.2", "Test stringS 3.2", 1, my_cat) == 
	"Тестовая строка 3.2", 
		"DCNgettext don't translate 1 form of string")

assert ( gettext.dcngettext(cat_domain, 
	"Test string 3.2", "Test stringS 3.2", 2, my_cat) == 
	"Тестовые строки 3.2", 
		"DCNgettext don't translate 2 form of string")

assert ( gettext.dcngettext(cat_domain, 
	"Test string 3.2", "Test stringS 3.2", 5, my_cat) == 
	"Тестовых строк 3.2", 
		"DCNgettext don't translate 3 form of string")

assert ( gettext.dcngettext(cat_domain, 
	"Untranslated string 3.2", "Untranslated stringS 3.2", 1, my_cat) == 
	"Untranslated string 3.2", 
		"DCNgettext translate 1 form of untranslated string")

assert ( gettext.dcngettext(cat_domain, 
	"Untranslated string 3.2", "Untranslated stringS 3.2", 12, my_cat) == 
	"Untranslated stringS 3.2", 
		"DCNgettext translate 2 form of untranslated string")
