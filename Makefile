LUA= lua5.2 
LUA_INTER= $(LUA)

HEADERS= lua-gettext.h category.list

CFLAGS+= -DPACKAGE='"lua"' -DLOCALEDIR='"."' 

CFLAGS+= -Wall -Wmissing-prototypes -Wmissing-declarations 
# -pedantic

CFLAGS+= $(shell pkg-config $(LUA) --cflags)
LDFLAGS+= $(shell pkg-config $(LUA) --libs)


SOFLAGS= -shared -fpic

.PHONY : all clean build test headers

all: build test

build: gettext.so

gettext.so: lua-gettext.c $(HEADERS)
	$(CC) $(SOFLAGS) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) $(TARGET_ARCH) \
		$< -o $@

test: gettext.so lua-gettext-test test.lua
	@./lua-gettext-test
	@ LANG=ru_RU.utf8 $(LUA_INTER) test.lua
	@echo "Everything is Ok"


lua-gettext-test: lua-gettext-test.o

lua-gettext-test.o: lua-gettext-test.c $(HEADERS)

clean: 
	-rm -f gettext.so lua-gettext-test *.o
