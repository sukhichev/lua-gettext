# lua-gettext is a Lua binding to GNU gettext package

[![Pipeline Status](https://gitlab.com/sukhichev/lua-gettext/badges/master/pipeline.svg)](https://gitlab.com/sukhichev/lua-gettext/-/commits/master)
[![Latest Release](https://gitlab.com/sukhichev/lua-gettext/-/badges/release.svg)](https://gitlab.com/sukhichev/lua-gettext/-/releases)

## Overview

lua-gettext is C module for Lua. You can use this module for 
internationalization (i18n) and/or localization (l10n) with
[GNU gettext](http://www.gnu.org/software/gettext/ "Go to GNU gettext site").

Support Lua 5.0 (not tested), Lua 5.1, Lua 5.2,  Lua 5.3 and Lua 5.4.

## Copyright

lua-gettext is dual-licensed free software. You can redistribute it 
and/or modify it under the terms of the GNU Lesser General Public 
License (LGPL) or Russian LGPL-like license (not implemented yet). 
Russian LGPL-like license need because Free Software Foundation (FSF) 
don't allow to make official LGPL translation.

## Download, build and install

You can download this package from my GitLab:
https://gitlab.com/sukhichev/lua-gettext

Debian and Ubuntu users can download binary packages of amd64 and 
source packages for some releases:

- Debian
  - [Buster](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=buster)
  - [Bullseye](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=bullseye)
  - [Bookworm](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=bookworm)
  - [Trixie](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=trixie)
  - [Sid](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=sid)

- Ubuntu
  - [Focal Fossa (20.04LTS)](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=focal)
  - [Jammy Jellyfish (22.04 LTS)](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=jammy)
  - [Noble Numbat (24.04 LTS)](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=noble)
  - [Oracular Minotaur (24.10)](https://gitlab.com/sukhichev/lua-gettext/-/jobs/artifacts/master/browse?job=oracular)

Also Debian and Ubuntu users can make deb package youself. 
All needed files included.

Run `make` after change `Makefile` if you need. Default target `all` 
automatic run target `test` for testing. Last string must be 
`Everything is Ok` or you have error(s). 

Tests of target `test` requiest `ru_RU.utf8` locale!

On Debian and Ubuntu you can run 

```shell
sudo dpkg-reconfigure locales
```

and choose `ru_RU.utf8` locale.

On other GNU/Linux uncomment string with `ru_RU.utf8` in 
`/etc/locale.gen` file and run `locale-gen`. May be need root.

Install and target `install` didn't implement yet.


## Implement functions list

* gettext.bindtextdomain
* gettext.textdomain
* gettext.gettext
* gettext.dgettext
* gettext.dcgettext
* gettext.ngettext
* gettext.dngettext
* gettext.dcngettext


Yes, I'am lazy ass, who don't write english part to end. Sad but true.
Use [russian description](README.md).
