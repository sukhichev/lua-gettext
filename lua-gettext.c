/*
 * This is Russian description. English description see below.
 * 
 * Привязка пакета GNU gettext для языка Lua
 * 
 * Copyright 2016-2024 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета lua-gettext.
 * 
 * О лицензии читай в файле lua-gettext.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * A Lua binding to GNU gettext package
 * 
 * Copyright 2016-2024 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of lua-gettext package.
 * 
 * See Copyright in lua-gettext.h
 *  
 */


#include <stdio.h>
#include <string.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include "lua-gettext.h"


static int get_category (const char * s)
{
	#define ADD_CAT(num)	{ num, #num }, 
	struct{
	  int 			category;
	  const char *	string;
	} category [] = {	
		#include "category.list"
		{ 0, NULL}
		};
	#undef ADD_CAT
	
	int i;
	
	for (i = 0; category[i].string != NULL; i++)
		if ( !strncmp(category[i].string, s, MAX_CAT_LEN) ) 
			return (category[i].category);
	return (CAT_ERROR);
}

/* 
 * 						textdomain functions series 					
 */

static int lua_textdomain(lua_State *L)
{
	if ( lua_isnil(L, 1) )
		lua_pushstring(L, textdomain( NULL ));
	else
		lua_pushstring(L, textdomain( luaL_checkstring(L, 1) ));// domain
	return (1);
}

static int lua_bindtextdomain(lua_State *L)
{
	lua_pushstring(L, bindtextdomain( luaL_checkstring(L, 1), // domain
									luaL_checkstring(L, 2) ));// dirname
	return (1);
}

/* 
 * 						gettext functions series 					
 */
static int lua_gettext(lua_State *L)
{
	lua_pushstring(L, gettext(luaL_checkstring(L, 1)));	// msgid
	return (1);
}

static int lua_dgettext(lua_State *L)
{
	lua_pushstring(L, dgettext( luaL_checkstring(L, 1), 	// domain
								luaL_checkstring(L, 2) ));	// msgid
	return (1);
}

static int lua_dcgettext(lua_State *L)
{
	int cat = get_category (luaL_checkstring(L, 3));	// category
	if (cat == CAT_ERROR)
		return (luaL_argerror(L, 3, "bad category name"));
	lua_pushstring(L, dcgettext(luaL_checkstring(L, 1),	// domain
								luaL_checkstring(L, 2),	// msgid
								cat ));
	return (1);
}

/* 
 * 						ngettext functions series 					
 */

static unsigned long int checkulong(lua_State *L, int arg)
{
#ifdef LUA_VERSION_NUM 
    #if LUA_VERSION_NUM >= 501 && LUA_VERSION_NUM <= 504 
        long n = (long) luaL_checkinteger(L, arg); 
    #else 
        #error "Undefinded Lua version"
    #endif
#else
    #ifdef LUA_COLIBNAME // need lualib.h
        long n = luaL_checklong(L, arg);
    #else 
        #error "Undefinded Lua version" 
    #endif 
#endif 
	if (n < 0) 
		return (luaL_argerror(L, arg, "n must be positive"));
	else
		return ( (unsigned long) n );
}

static int lua_ngettext(lua_State *L)
{
	unsigned long n = checkulong(L, 3);
	lua_pushstring(L, ngettext( luaL_checkstring(L, 1),	// msgid1
								luaL_checkstring(L, 2),	// msgid2
								n ));
	return (1);
}

static int lua_dngettext(lua_State *L)
{
	unsigned long n = checkulong(L, 4);
	lua_pushstring(L, dngettext( luaL_checkstring(L, 1),	// domain
								luaL_checkstring(L, 2),	// msgid1
								luaL_checkstring(L, 3),	// msgid2
								n ));
	return (1);
}

static int lua_dcngettext(lua_State *L)
{
	unsigned long n = checkulong(L, 4);
	int cat = get_category (luaL_checkstring(L, 5));	// category
	if (cat == CAT_ERROR)
		return (luaL_argerror(L, 5, "bad category name"));
		
	lua_pushstring(L, dcngettext( luaL_checkstring(L, 1),	// domain
								luaL_checkstring(L, 2),	// msgid1
								luaL_checkstring(L, 3),	// msgid2
								n,
								cat ));
	return (1);
}



const luaL_Reg gettext_funct[] = {
	{"bindtextdomain",	lua_bindtextdomain },
	{"textdomain",	lua_textdomain },
	
	{"gettext",		lua_gettext },
	{"dgettext",	lua_dgettext },
	{"dcgettext",	lua_dcgettext },
	{"ngettext",	lua_ngettext },
	{"dngettext",	lua_dngettext },
	{"dcngettext",	lua_dcngettext },
	/*pgettext ???*/
	{NULL, NULL}
};



MYMOD_API int luaopen_gettext(lua_State *L)
{
    // Init
    setlocale (LC_ALL, "");
    bindtextdomain (PACKAGE, LOCALEDIR);
    textdomain (PACKAGE);
    
    lua_register (L, "_", lua_gettext);
    
#ifdef LUA_VERSION_NUM
    #if LUA_VERSION_NUM == 501
        luaL_register(L, "gettext", gettext_funct); // Lua 5.1
    #elif LUA_VERSION_NUM >= 502 && LUA_VERSION_NUM <= 504
        luaL_newlib(L, gettext_funct); // Lua 5.2 -- 5.4        
    #else
        #error "Undefinded Lua version"
    #endif
#else
    #ifdef LUA_COLIBNAME // need lualib.h
        luaL_openlib(L, "gettext", gettext_funct, 0); // Lua 5.0
    #else
        #error "Undefinded Lua version"   
    #endif
#endif
    
    return(1);
}
