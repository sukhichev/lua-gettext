/*
 * This is Russian description. English description see below.
 * 
 * Часть тестов для пакета lua-gettext на языке C
 * 
 * Copyright 2016 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета lua-gettext.
 * 
 * О лицензии читай в файле lua-gettext.h
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * C part of tests for lua-gettext package
 * 
 * Copyright 2016 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 * 
 * This file is part of lua-gettext package.
 * 
 * See Copyright in lua-gettext.h
 *  
 */


#include <stdio.h>
#include <string.h>

#include "lua.h"
#include "lua-gettext.h"


int main(int argc, char **argv)
{
	// Unsafe constant test
	#define ADD_CAT(num)	{ num, #num }, 
	struct{
	  int 			category;
	  const char *	string;
	} category [] = {	
		#include "category.list"
		{ 0, NULL}
		};
	#undef ADD_CAT
	
	int i;
	
	for (i = 0; category[i].string != NULL; i++)
	{
		// Test CAT_ERROR
		if ( category[i].category == CAT_ERROR ) 
		{
			fprintf (stderr, 
				"Bad CAT_ERROR value. '%s' using this value\n",
				category[i].string);
			return (-1);
		}
		// Test MAX_CAT_LEN
		if ( strlen(category[i].string) >= MAX_CAT_LEN ) 
		{
			fprintf (stderr, 
				"MAX_CAT_LEN value too small. Need %zd, set %d\n",
				strlen(category[i].string), MAX_CAT_LEN);
			return (-1);
		}
	}
	
	return 0;
}

