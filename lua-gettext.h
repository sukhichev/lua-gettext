/*
 * This is Russian description. English description see below.
 * 
 * Привязка пакета GNU gettext для языка Lua
 * 
 * Copyright 2016 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * Этот файл -- часть пакета lua-gettext.
 * 
 * Это свободная программа. Вы можете распространять, модифицировать,
 * или выполнять эти действия одновременно на условиях
 * 1) cтандартной общественной лицензии ограниченного применения GNU 
 * (LGPL), выпущенной  Фондом свободного программного обеспечения (FSF)
 * версии 2.1, или (на ваше усмотрение) любой более поздней версии;
 * 		ИЛИ
 * 2) русскоязычной LGPL-подобной лицензии (пока ещё не написана)
 * 
 * Эта программа распространяется в надежде, что она будет полезной,
 * но без каких-либо явных или подразумеваемых гарантии. Подробнее 
 * смотри cтандартную общественную лицензию ограниченного применения 
 * GNU (LGPL) и русскоязычную LGPL-подобную лицензию.
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 * A Lua binding to GNU gettext package
 * 
 * Copyright 2016 Сухичев Михаил Иванович <sukhichev@yandex.ru>
 *  
 * This file is part of lua-gettext package.
 * 
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of
 * 1) the GNU Lesser General Public License (LGPL) as published by the 
 * Free Software Foundation (FSF); either version 2.1 of the License, or 
 * (at your option) any later version;
 * 		OR
 * 2) Russian LGPL-like license (not implemented yet)
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License or Russian LGPL-like license
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307 USA.
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  */

#if (__NetBSD__ || __linux__)
#include <libintl.h>
#include <locale.h>
#else
#include "libintl.h"
#include "locale.h"
#endif


// Default contants
#ifndef PACKAGE
#define PACKAGE		"lua"
#endif
#ifndef LOCALEDIR
#define LOCALEDIR	"."
#endif

// Unsafe constant
#define CAT_ERROR		-1 /* May be conflict with category values*/
#define MAX_CAT_LEN		20 /* May be so short for category name*/

// Export functions
#define MYMOD_API	LUA_API

MYMOD_API int luaopen_gettext(lua_State *L);
